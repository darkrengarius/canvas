package primitive

// FloodFill describes flood fill operation parameters.
type FloodFill struct {
	Start Coords `json:"start"`
	Fill  byte   `json:"fill"`
}
