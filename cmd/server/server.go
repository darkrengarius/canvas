package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"gitlab.com/darkrengarius/canvas/internal/api"
	"gitlab.com/darkrengarius/canvas/pkg/canvas"
	"gitlab.com/darkrengarius/canvas/pkg/db"
	"gitlab.com/darkrengarius/canvas/pkg/redisrepo"
)

const (
	defaultHost      = "127.0.0.1"
	defaultPort      = 5000
	defaultRedisHost = "127.0.0.1"
	defaultRedisPort = 6379
)

var defaultCanvasCfg = canvas.DefaultConfig()

var (
	host          string
	port          int
	redisHost     string
	redisPort     int
	redisPassword string
	canvasWidth   int
	canvasHeight  int
)

func init() {
	rootCmd.Flags().StringVar(&host, "host", defaultHost, "host to bind to")
	rootCmd.Flags().IntVar(&port, "port", defaultPort, "port to bind to")
	rootCmd.Flags().StringVar(&redisHost, "redis-host", defaultRedisHost, "Redis host")
	rootCmd.Flags().IntVar(&redisPort, "redis-port", defaultRedisPort, "Redis port")
	rootCmd.Flags().StringVar(&redisPassword, "redis-password", "", "Redis password")
	rootCmd.Flags().IntVar(&canvasWidth, "canvas-width", defaultCanvasCfg.W, "default canvas width")
	rootCmd.Flags().IntVar(&canvasHeight, "canvas-height", defaultCanvasCfg.H, "default canvas height")
}

var rootCmd = &cobra.Command{
	Use:   "server",
	Short: "ASCII Canvas Server",
	Run: func(_ *cobra.Command, args []string) {
		logger := logrus.New()

		if canvasWidth > 0 {
			defaultCanvasCfg.W = canvasWidth
		}

		if canvasHeight > 0 {
			defaultCanvasCfg.H = canvasHeight
		}

		redisCfg := db.RedisConfiguration{
			Host:     redisHost,
			Port:     redisPort,
			Password: redisPassword,
		}

		redis, err := db.ConnectToRedis(redisCfg)
		if err != nil {
			logger.WithError(err).Fatalln("Failed to connect to Redis")
		}

		canvasRepo := redisrepo.NewCanvasRepository(redis)

		canvasService := canvas.NewService(defaultCanvasCfg, canvasRepo)

		apiCfg := api.Config{
			Host: host,
			Port: port,
		}

		a, err := api.New(apiCfg, canvasService)
		if err != nil {
			logger.WithError(err).Fatalln("Failed to create API instance")
		}

		osSigs := make(chan os.Signal)
		signal.Notify(osSigs, syscall.SIGTERM)
		signal.Notify(osSigs, syscall.SIGINT)

		go func() {
			<-osSigs
			if err := a.Shutdown(context.Background()); err != nil {
				logger.WithError(err).Errorln("Error during API shutdown")
			}
		}()

		if err := a.ListenAndServe(); err != nil {
			logger.WithError(err).Errorln("Failed to server requests")
		}
	},
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Printf("Failed to run server: %v\n", err)
	}
}
