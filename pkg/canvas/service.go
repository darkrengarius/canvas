package canvas

import "errors"

var (
	// ErrInvalidCanvasID is returned if specified canvas ID is invalid.
	ErrInvalidCanvasID = errors.New("invalid canvas ID")
)

// Service is a stored canvases manipulating service.
type Service struct {
	canvasCfg Config
	repo      Repository
}

// NewService constructs new canvas service.
func NewService(canvasCfg Config, repo Repository) *Service {
	return &Service{
		canvasCfg: canvasCfg,
		repo:      repo,
	}
}

// Get gets canvas by its `id`.
func (s *Service) Get(id string) (*Canvas, error) {
	if id == "" {
		return nil, ErrInvalidCanvasID
	}

	return s.get(id)
}

// GetOrCreate gets canvas by its `id` if it's not empty. Creates
// new canvas otherwise.
func (s *Service) GetOrCreate(id string) (*Canvas, error) {
	if id == "" {
		return New(s.canvasCfg), nil
	}

	return s.get(id)
}

// AddOrUpdate updates canvas by its `id` if it's not empty. Adds
// new canvas otherwise.
func (s *Service) AddOrUpdate(id string, c *Canvas) (string, error) {
	if id == "" {
		return s.repo.Add(c)
	}

	return id, s.repo.Update(id, c)
}

func (s *Service) get(id string) (*Canvas, error) {
	c, err := s.repo.Canvas(id)
	if err != nil {
		return nil, err
	}

	c.SetFloodFiller(s.canvasCfg.FF)

	return c, nil
}
