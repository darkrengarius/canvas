package stack

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewSliceStack(t *testing.T) {
	s := NewSliceStack()
	require.NotNil(t, s)
	require.Nil(t, s.data)
}

func TestSliceStack_Push(t *testing.T) {
	s := NewSliceStack()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		s.Push(v)
	}

	require.Len(t, s.data, len(data))
	for i := range data {
		require.Equal(t, data[i], s.data[i])
	}
}

func TestSliceStack_Peek(t *testing.T) {
	s := NewSliceStack()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		s.Push(v)
	}

	for i := len(data) - 1; i >= 0; i-- {
		require.Equal(t, data[i], s.Peek())
		s.data = s.data[:len(s.data)-1]
	}

	require.Nil(t, s.Peek())
}

func TestSliceStack_Pop(t *testing.T) {
	s := NewSliceStack()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		s.Push(v)
	}

	for i := len(data) - 1; i >= 0; i-- {
		require.Equal(t, data[i], s.Pop())
	}

	require.Nil(t, s.Peek())
}

func TestSliceStack_Clear(t *testing.T) {
	s := NewSliceStack()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		s.Push(v)
	}

	s.Clear()
	require.NotNil(t, s.data)
	require.Len(t, s.data, 0)
}
