package canvas

type marshalable struct {
	W    int      `json:"w"`
	H    int      `json:"h"`
	Data [][]byte `json:"data"`
}

func (m *marshalable) toCanvas() *Canvas {
	return &Canvas{
		w:    m.W,
		h:    m.H,
		data: m.Data,
	}
}
