package canvas

//go:generate mockery -name Repository -case underscore -inpkg

// Repository is a storage of canvases.
type Repository interface {
	// Canvas gets stored canvas by its `id`.
	Canvas(id string) (*Canvas, error)

	// Add stores new canvas.
	Add(c *Canvas) (string, error)

	// Update updates existing canvas by its `id`.
	Update(id string, c *Canvas) error
}
