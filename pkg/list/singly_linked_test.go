package list

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewSinglyLinked(t *testing.T) {
	l := NewSinglyLinked()

	require.NotNil(t, l)
	require.Nil(t, l.head)
	require.Nil(t, l.tail)
}

func TestSinglyLinked_Head(t *testing.T) {
	t.Run("empty list", func(t *testing.T) {
		l := NewSinglyLinked()

		require.Nil(t, l.Head())
	})

	t.Run("not empty list", func(t *testing.T) {
		l := NewSinglyLinked()

		wantData := 1

		n := &node{
			data: wantData,
		}

		l.head = n
		l.tail = l.head

		gotData := l.Head()
		require.Equal(t, wantData, gotData)
	})
}

func TestSinglyLinked_Tail(t *testing.T) {
	t.Run("empty list", func(t *testing.T) {
		l := NewSinglyLinked()

		require.Nil(t, l.Tail())
	})

	t.Run("not empty list", func(t *testing.T) {
		l := NewSinglyLinked()

		wantData := 1

		n := &node{
			data: wantData,
		}

		l.head = n
		l.tail = l.head

		gotData := l.Tail()
		require.Equal(t, wantData, gotData)
	})
}

func TestSinglyLinked_AddFirst(t *testing.T) {
	l := NewSinglyLinked()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		l.AddFirst(v)
	}

	i := len(data) - 1
	for ptr := l.head; ptr != nil; ptr = ptr.next {
		require.Equal(t, data[i], ptr.data)
		i--
	}
}

func TestSinglyLinked_AddLast(t *testing.T) {
	l := NewSinglyLinked()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		l.AddLast(v)
	}

	i := 0
	for ptr := l.head; ptr != nil; ptr = ptr.next {
		require.Equal(t, data[i], ptr.data)
		i++
	}
}

func TestSinglyLinked_RemoveFirst(t *testing.T) {
	l := NewSinglyLinked()

	l.RemoveFirst()
	require.Nil(t, l.head)
	require.Nil(t, l.tail)

	data := []interface{}{
		1,
		"data",
	}

	for _, v := range data {
		l.AddLast(v)
	}

	l.RemoveFirst()
	require.Equal(t, data[1], l.Head())
	require.Nil(t, l.head.next)

	l.RemoveFirst()
	require.Nil(t, l.head)
	require.Nil(t, l.tail)
}
