package simplefloodfiller

import (
	"gitlab.com/darkrengarius/canvas/pkg/primitive"
	"gitlab.com/darkrengarius/canvas/pkg/stack"
)

// coordsStack wraps `stack.SliceStack` to simplify accessing
// stack of coordinates.
type coordsStack struct {
	s *stack.SliceStack
}

func newCoordsStack() *coordsStack {
	return &coordsStack{
		s: stack.NewSliceStack(),
	}
}

func (s *coordsStack) push(v primitive.Coords) {
	s.s.Push(v)
}

func (s *coordsStack) pop() (primitive.Coords, bool) {
	v := s.s.Pop()
	if v == nil {
		return primitive.Coords{}, false
	}

	return v.(primitive.Coords), true
}

func (s *coordsStack) clear() {
	s.s.Clear()
}
