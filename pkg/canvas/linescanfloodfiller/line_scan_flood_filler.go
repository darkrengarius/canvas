// Package linescanfloodfiller provides implementation of `canvas.FloodFiller`
// which utilizes line scanning.
package linescanfloodfiller

import "gitlab.com/darkrengarius/canvas/pkg/primitive"

// LineScanFloodFiller implements `canvas.FloodFiller`
// utilizing line scanning.
type LineScanFloodFiller struct {
}

// New constructs new `LineScanFloodFiller`.
func New() *LineScanFloodFiller {
	return &LineScanFloodFiller{}
}

// FloodFill flood-fills matrix `m` replacing encountered `replaceChar`s with `fillChar`.
// Uses line scanning to fill matrix.
// Implements `canvas.FloodFiller`.
func (f *LineScanFloodFiller) FloodFill(m [][]byte, start primitive.Coords, replaceChar, fillChar byte) {
	if start.Y < 0 || start.Y >= len(m) ||
		start.X < 0 || start.X >= len(m[start.Y]) ||
		m[start.Y][start.X] != replaceChar {
		return
	}

	q := newScannedLinesQueue()
	q.push(scannedLine{
		startX:     start.X,
		endX:       start.X,
		y:          start.Y,
		movedFromY: -1,
	})

	for line, ok := q.pop(); ok; line, ok = q.pop() {
		x := line.startX
		for x <= line.endX {
			// loop over the line till we encounter first element to be replaced
			if m[line.y][x] != replaceChar {
				x++
				continue
			}

			// fill available part of the line and get its start and end
			subLineStart, subLineEnd := f.fillLineFrom(m, x, line.y, replaceChar, fillChar)
			if subLineStart >= line.startX && subLineEnd <= line.endX && line.movedFromY != -1 {
				// in this case our sub line lies completely within the scanned line

				if line.movedFromY < line.y && (line.y+1) < len(m) {
					// we were moving down, so keep on
					q.push(scannedLine{
						startX:     subLineStart,
						endX:       subLineEnd,
						y:          line.y + 1,
						movedFromY: line.y,
					})
				}

				if line.movedFromY > line.y && line.y > 0 {
					// we were moving up, so keep on
					q.push(scannedLine{
						startX:     subLineStart,
						endX:       subLineEnd,
						y:          line.y - 1,
						movedFromY: line.y,
					})
				}
			} else {
				// in this case sub line extends scanned line's borders either left or right, or both.
				// this actually means, that we need to move further keeping the original direction, but
				// also need to move backwards

				if line.y > 0 {
					q.push(scannedLine{
						startX:     subLineStart,
						endX:       subLineEnd,
						y:          line.y - 1,
						movedFromY: line.y,
					})
				}

				if (line.y + 1) < len(m) {
					q.push(scannedLine{
						startX:     subLineStart,
						endX:       subLineEnd,
						y:          line.y + 1,
						movedFromY: line.y,
					})
				}
			}

			// sub line ended at `subLineEnd`, this means that `subLineEnd+1` definitely
			// can't be filled, so we skip it with `+2`
			x = subLineEnd + 2
		}
	}
}

// fillLineFrom fills line left and right starting from and including element at [x,y].
func (f *LineScanFloodFiller) fillLineFrom(m [][]byte, x, y int, replaceChar, fillChar byte) (int, int) {
	m[y][x] = fillChar

	lineStartX := x - 1
	for lineStartX >= 0 && m[y][lineStartX] == replaceChar {
		m[y][lineStartX] = fillChar
		lineStartX--
	}
	lineStartX++

	lineEndX := x + 1
	for lineEndX < len(m[y]) && m[y][lineEndX] == replaceChar {
		m[y][lineEndX] = fillChar
		lineEndX++
	}
	lineEndX--

	return lineStartX, lineEndX
}
