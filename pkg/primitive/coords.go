package primitive

// Coords is a pair of coordinates within 2D matrix.
type Coords struct {
	X int `json:"x"`
	Y int `json:"y"`
}
