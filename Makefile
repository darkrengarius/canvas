dep:
	go mod tidy
	go mod vendor -v

generate:
	PATH=$$PATH:$$HOME/go/bin go generate ./pkg/...

clean:
	rm -rf ./bin
	mkdir ./bin

build: clean
	go build -mod=vendor -o ./bin/server/server ./cmd/server
	cp ./cmd/server/Dockerfile ./bin/server/

test:
	go test -race ./...

lint:
	golangci-lint run -c .golangci.yml ./...

run: clean
	GOOS=linux GOARCH=amd64 go build -mod=vendor -tags=netgo -o ./bin/server/server ./cmd/server
	cp ./cmd/server/Dockerfile ./bin/server/
	docker build -t canvas-server ./bin/server
	bash -c "docker-compose build"
	bash -c "docker-compose up -d"
	bash -c "docker-compose ps"

stop:
	bash -c "docker-compose stop"
	bash -c "docker-compose ps"
	bash -c "docker-compose down"

