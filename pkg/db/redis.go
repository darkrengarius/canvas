package db

import (
	"fmt"

	"github.com/go-redis/redis"
)

// ConnectToRedis connects to the Redis instance.
func ConnectToRedis(c RedisConfiguration) (redis.UniversalClient, error) {
	if c.Host == "" {
		return nil, ErrNoHost
	}

	if c.Port <= 0 {
		return nil, ErrNoPort
	}

	cl := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", c.Host, c.Port),
		Password: c.Password,
	})

	if err := cl.Ping().Err(); err != nil {
		return nil, fmt.Errorf("failed to perform Ping: %w", err)
	}

	return cl, nil
}
