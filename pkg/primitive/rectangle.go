package primitive

// Rectangle describes rectangle to draw.
type Rectangle struct {
	// Start points at left upper corner of the rectangle.
	Start   Coords `json:"start"`
	Width   int    `json:"width"`
	Height  int    `json:"height"`
	Fill    *byte  `json:"fill"`
	Outline *byte  `json:"outline"`
}

// IsValid checks rectangle `r` for validity.
func (r Rectangle) IsValid() bool {
	return r.Fill != nil || r.Outline != nil
}
