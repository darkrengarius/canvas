# Canvas

ASCII Canvas Service allows to draw on the canvas with ASCII characters. Contains server and client.

## Note

I wanted to experiment with flood filling algorithm and bumped into the line scanning one. Implemented it along with the simple basic version and did benchmarks. Using linked lists as a backend storage for data structures makes both of these algorithms complete at around the sime time, so I decided to stick with the basic implementation. However, I left both here, so packages `list` and `queue` are only used for the line scanning algorithm. Algorithm may be switched in `pkg/canvas/config.go` in function `DefaultConfig`.

## Requirements

- Go 1.13+
- Redis (to store canvases)
- Docker + compose (for easier launch, not required)

## Server

Server uses Redis as a canvas storage. I chose Redis, cause it's simple to setup, to use, it's schema-less and gets job done.

Server binary supports several configuration flags:

- `--host`: host to bind to (defaults to `localhost`);
- `--port`: port to bind to (defaults to `5000`);
- `--redis-host`: Redis host (defaults to `localhost`);
- `--redis-port`: Redis port (defaults to `6379);
- `--redis-password`: Redis password (defaults to empty, therefore is not required);
- `--canvas-width`: canvas width (defaults to `50`);
- `--canvas-height`: canvas height (defaults to `50`).

### Tests

Tests may be run via `make test`. Basically Goland shows around 60% coverage of the overall the project. 

### How to Launch

Server may be easily built with `make build`. This command will but `sernver` binary under `./bin/server/`. This will allow to start right away if you have Redis ready. In this case, just pass Redis parameters to the binary as flags. Additionally `Dockerfile` is put to the same folder.

### How to Launch (easy lazy way)

In case you have docker and docker-compose installed, just run `make run`. This will run 2 containers: Redis and the serer itself. Server will be then available via port 5000. Do not forget to turn it off with `make stop` afterwards.

## Client

I am not that much into frontend and I had some pretty old code pieces which I wrote a couple of years ago, so I decided to re-use these and not to involve any modern JS framework. So, rendering is done with the help of jQuery, despite of it being pretty much outdated nowadays.

### How To Launch

1. Ensure that server is running
2. Send either flood-fill or put-rectangle request to the server to create canvas and get its ID 
3. Go to `./static/js/scripts.js`, there's a `canvasId` constant definition on line 15, put the canvas ID from p.2 there
4. Run `./static/index.html` in any modern browser