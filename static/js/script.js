function renderCanvas(renderedRows) {
    $("#content").html(`
        <table id="canvas-table">
        
        </table>`);
    for (let i = 0; i < renderedRows.length; i++) {
        $("#canvas-table").append(`
            <tr>
                <td><pre>${renderedRows[i]}</pre></td>
            </tr>
        `)
    }
}

const canvasId = "";
let client = new JSONRPCClient();

$(document).ready(function () {
    client.connect("ws://127.0.0.1:5000/ws").then(() => {
        console.log("connected via ws");
        client.sendCanvasId(canvasId);
    });
});