package list

// SinglyLinked is a singly linked list.
type SinglyLinked struct {
	head *node
	tail *node
}

// NewSinglyLinked constructs new singly linked list.
func NewSinglyLinked() *SinglyLinked {
	return &SinglyLinked{}
}

// Head gets data from the head of the list.
func (l *SinglyLinked) Head() interface{} {
	if l.head == nil {
		return nil
	}

	return l.head.data
}

// Tail gets data from the tail of the list.
func (l *SinglyLinked) Tail() interface{} {
	if l.tail == nil {
		return nil
	}

	return l.tail.data
}

// AddFirst adds data to the head of the list.
func (l *SinglyLinked) AddFirst(v interface{}) {
	n := &node{
		data: v,
		next: l.head,
	}

	if l.head == nil {
		l.tail = n
	}

	l.head = n
}

// AddLast adds data to the tail of the list.
func (l *SinglyLinked) AddLast(v interface{}) {
	n := &node{
		data: v,
	}

	if l.head == nil {
		l.head = n
		l.tail = l.head
	} else {
		l.tail.next = n
		l.tail = n
	}
}

// RemoveFirst removes data from the head of the list.
func (l *SinglyLinked) RemoveFirst() {
	if l.head == nil {
		return
	}

	l.head = l.head.next
	if l.head == nil {
		l.tail = nil
	}
}
