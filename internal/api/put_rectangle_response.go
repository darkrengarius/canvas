package api

// PutRectangleResponse is a response for `PutRectangle`
// drawing operation.
type PutRectangleResponse struct {
	CanvasID string `json:"canvas_id"`
}
