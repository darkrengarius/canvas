package stack

// SliceStack is a stack utilizing slice as a
// base data storage.
type SliceStack struct {
	data []interface{}
}

// NewSliceStack constructs new `ListQueue`.
func NewSliceStack() *SliceStack {
	return &SliceStack{}
}

// Push adds item `v` to the top of the stack.
func (s *SliceStack) Push(v interface{}) {
	s.data = append(s.data, v)
}

// Peek gets item from the top of the stack.
func (s *SliceStack) Peek() interface{} {
	if len(s.data) == 0 {
		return nil
	}

	return s.data[len(s.data)-1]
}

// Pop gets and removes item from the top of the queue.
func (s *SliceStack) Pop() interface{} {
	v := s.Peek()
	if v == nil {
		return nil
	}

	s.data = s.data[:len(s.data)-1]

	return v
}

// Clear clears out array contents.
func (s *SliceStack) Clear() {
	s.data = s.data[:0]
}
