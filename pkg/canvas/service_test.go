package canvas

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/darkrengarius/canvas/pkg/canvas/simplefloodfiller"
)

func TestNewService(t *testing.T) {
	canvasCfg := DefaultConfig()
	repo := &MockRepository{}

	s := NewService(canvasCfg, repo)
	require.NotNil(t, s)
	require.Equal(t, canvasCfg, s.canvasCfg)
	require.Equal(t, repo, s.repo)
}

func TestService_get(t *testing.T) {
	type want struct {
		canvas *Canvas
		err    error
	}

	canvasCfg := Config{
		W:  30,
		H:  30,
		FF: simplefloodfiller.New(),
	}
	repoReturnedCanvas := New(canvasCfg)

	tests := []struct {
		name               string
		canvasID           string
		repoReturnedCanvas *Canvas
		repoReturnedErr    error
		want               want
	}{
		{
			name:               "ok",
			canvasID:           "1",
			repoReturnedErr:    nil,
			repoReturnedCanvas: repoReturnedCanvas,
			want: want{
				canvas: repoReturnedCanvas,
				err:    nil,
			},
		},
		{
			name:               "internal repo error",
			canvasID:           "2",
			repoReturnedErr:    assert.AnError,
			repoReturnedCanvas: nil,
			want: want{
				canvas: nil,
				err:    assert.AnError,
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			repo := &MockRepository{}
			repo.On("Canvas", tc.canvasID).Return(tc.repoReturnedCanvas, tc.repoReturnedErr)

			s := NewService(canvasCfg, repo)
			c, err := s.get(tc.canvasID)
			require.Equal(t, tc.want.err, err)
			if err != nil {
				return
			}

			require.Equal(t, tc.want.canvas, c)
		})
	}
}

func TestService_Get(t *testing.T) {
	canvasCfg := Config{
		W:  30,
		H:  30,
		FF: simplefloodfiller.New(),
	}

	t.Run("ok", func(t *testing.T) {
		canvasID := "1"
		repoReturnedCanvas := New(canvasCfg)
		var repoReturnedErr error

		repo := &MockRepository{}
		repo.On("Canvas", canvasID).Return(repoReturnedCanvas, repoReturnedErr)

		s := NewService(canvasCfg, repo)
		c, err := s.Get(canvasID)
		require.Nil(t, err)
		require.Equal(t, repoReturnedCanvas, c)
	})

	t.Run("invalid id", func(t *testing.T) {
		s := NewService(canvasCfg, nil)
		c, err := s.Get("")
		require.Equal(t, ErrInvalidCanvasID, err)
		require.Nil(t, c)
	})
}

func TestService_GetOrCreate(t *testing.T) {
	canvasCfg := Config{
		W:  30,
		H:  30,
		FF: simplefloodfiller.New(),
	}

	t.Run("get existing canvas", func(t *testing.T) {
		canvasID := "1"
		repoReturnedCanvas := New(canvasCfg)
		var repoReturnedErr error

		repo := &MockRepository{}
		repo.On("Canvas", canvasID).Return(repoReturnedCanvas, repoReturnedErr)

		s := NewService(canvasCfg, repo)
		c, err := s.GetOrCreate(canvasID)
		require.Nil(t, err)
		require.Equal(t, repoReturnedCanvas, c)
	})

	t.Run("create new canvas", func(t *testing.T) {
		s := NewService(canvasCfg, nil)
		c, err := s.GetOrCreate("")
		require.Nil(t, err)
		require.NotNil(t, c)
		require.Equal(t, canvasCfg.W, c.w)
		require.Equal(t, canvasCfg.H, c.h)
		require.Equal(t, canvasCfg.FF, c.floodFiller)
	})
}

func TestService_AddOrUpdate(t *testing.T) {
	canvasCfg := Config{
		W:  30,
		H:  30,
		FF: simplefloodfiller.New(),
	}

	t.Run("add new canvas", func(t *testing.T) {
		canvasToAdd := New(canvasCfg)
		repoReturnedID := "1"
		var repoReturnedErr error

		repo := &MockRepository{}
		repo.On("Add", canvasToAdd).Return(repoReturnedID, repoReturnedErr)

		s := NewService(canvasCfg, repo)
		id, err := s.AddOrUpdate("", canvasToAdd)
		require.Equal(t, repoReturnedErr, err)
		require.Equal(t, repoReturnedID, id)
	})

	t.Run("update existing canvas", func(t *testing.T) {
		canvasToUpdate := New(canvasCfg)
		repoReturnedID := "1"
		var repoReturnedErr error

		repo := &MockRepository{}
		repo.On("Update", repoReturnedID, canvasToUpdate).Return(repoReturnedErr)

		s := NewService(canvasCfg, repo)
		id, err := s.AddOrUpdate(repoReturnedID, canvasToUpdate)
		require.Equal(t, repoReturnedErr, err)
		require.Equal(t, repoReturnedID, id)
	})
}
