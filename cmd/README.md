# API

Canvas first gets created on the initial drawing operation on it if no canvas ID is specified in the request. Both flood fill and rectangle operations create a canvas in this case.

If JSON is passed to the server in the request, content type header with `application/json` is required.

- [API](#api)
  - [Drawing](#drawing)
    - [PutRectangle](#put-rectangle)
    - [FloodFill](#flood-fill)
  - [Canvas](#canvas)
    - [GetCanvas](#get-canvas)
  - [WS](#ws)
    - [SubscribeForCanvasUpdates](#subscribe-for-canvas-updates)

## Drawing

### Put Rectangle

POST /api/rectangles[?canvas_id=]

This endpoint puts rectangle to the canvas. If `canvas_id` is specified, it fetches existing canvas and modifies it. Otherwise, new canvas gets created.

Query parameters:
- canvas_id (string).

Body (JSON):
- Rectangle:
    - start (Coords): coordinates of upper left corner;
    - width (int): rectangle width;
    - height (int): rectangle height;
    - fill (byte): ASCII code of the character to fill the rectangle body;
    - outline (byte): ASCII code of the character to draw the rectangle border.

- Coords:
    - x (int);
    - y (int).
    
If rectangle has `fill` specified, but `outline` omitted, border will be drawn with `fill`.

If start of the rectangle is out of the canvas borders, it's not being drawn, no error returned. If part of the rectangle is out of the canvas borders, but its `start` is within the canvas, it gets drawn from the start till the canvas borders.

Specific status codes:
- 200: operation was applied to the canvas;
- 400: rectangle is not valid. It's considered valid if either `fill` or `outline` is specified; 
- 404: happens only if `canvas_id` is specified and canvas with such ID was not found.

Response (JSON):
- canvas_id (string): ID of the canvas. If ID was passed in query, the same one will be returned here.

Request body example:

```json
{
    "start": {
        "x": 3,
        "y": 7
    },
    "width": 6,
    "height": 3,
    "outline": 88
}
```

Response body example:

```json
{
    "canvas_id": "db1c1ad7-3e1e-46ad-a413-b9112c0ea9c4"
}
```

### Flood Fill

POST /api/flood-fills[?canvas_id=]

This endpoint flood-fills the canvas. If `canvas_id` is specified, it fetches existing canvas and modifies it. Otherwise, new canvas gets created.

Query parameters:
- canvas_id (string).

Body (JSON):
- FloodFill:
    - start (Coords): coordinates of upper left corner;
    - fill (byte): ASCII code of the character to fill the canvas.

- Coords:
    - x (int);
    - y (int).

Canvas gets flood-filled starting from the point `start` spreading in 4 directions: up, right, down and left. If flood fill encounters character, which is different from the one that's been initially in the `start`, it stops moving in that direction.    

Specific status codes:
- 200: operation was applied to the canvas;
- 404: happens only if `canvas_id` is specified and canvas with such ID was not found.

Response (JSON):
- canvas_id (string): ID of the canvas. If ID was passed in query, the same one will be returned here.

Request body example:

```json
{
    "start": {
        "x": 3,
        "y": 7
    },
    "fill": 65
}
```

Response body example:

```json
{
    "canvas_id": "db1c1ad7-3e1e-46ad-a413-b9112c0ea9c4"
}
```

## Canvas

### Get Canvas

GET /api/canvases/{canvas_id}

Gets canvas by its ID.

Query parameters:
- canvas_id (string).

Specific status codes:
- 200: operation was applied to the canvas;
- 404: happens if canvas with such ID was not found.

Response (JSON):
- rendered ([]string): the rendered array of canvas rows. Each row contains the row of the canvas converted to string.

Response body example:

```json
{
    "rendered": [
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "   XXXXXX                                         ",
        "   X    X                                         ",
        "   X    X                                         ",
        "   XXXXXX                                         ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  "
    ]
}
```

## WS

There is a small websocket API which allows to get canvas updates in real time. One may initiate WS connection by sending the request to `/ws`.

After connecting over WS, server starts sending to the client `ping` messages to detect connection failure. For the sake of simplicity server doesn't expect any `pong`s in response.

### Subscribe For Canvas Updates

After connection is established, client must send in the ID of the canvas it needs to receive updates for. 

Request (JSON):
- canvas_id (string).

Request body example:

```json
{
    "canvas_id": "db1c1ad7-3e1e-46ad-a413-b9112c0ea9c4"
}
```

Right after that client may expect messages with the canvas update. Messages come in each time canvas gets changed.

Update message (JSON):
- rendered ([]string): the rendered array of canvas rows. Each row contains the row of the canvas converted to string.

This message is the same as in `GetCanvas` endpoint.

Example:

```json
{
    "rendered": [
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "   XXXXXX                                         ",
        "   X    X                                         ",
        "   X    X                                         ",
        "   XXXXXX                                         ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  ",
        "                                                  "
    ]
}
```
