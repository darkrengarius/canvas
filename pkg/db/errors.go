package db

import "errors"

var (
	// ErrNoHost is returned if no host provided.
	ErrNoHost = errors.New("no host specified")
	// ErrNoPort is returned if no port provided.
	ErrNoPort = errors.New("no port specified")
)

var (
	// ErrNil is returned if no data found in the DB.
	ErrNil = errors.New("no data")
)
