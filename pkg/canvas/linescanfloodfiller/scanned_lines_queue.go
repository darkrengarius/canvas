package linescanfloodfiller

import "gitlab.com/darkrengarius/canvas/pkg/queue"

// scannedLine is a line which was filled on the previous step of the algorithm.
// Each line means that at least between `startX` and `endX` inclusively on
// row `y` we need to check for elements to be filled. Also this range might be
// extended for during check on the current row. `movedFromY` is used to control
// the direction of movement over the matrix - up or down.
type scannedLine struct {
	startX     int
	endX       int
	y          int
	movedFromY int
}

// scannedLinesQueue wraps `queue.Queue` to simplify accessing
// queue of scanned lines.
type scannedLinesQueue struct {
	q *queue.ListQueue
}

func newScannedLinesQueue() *scannedLinesQueue {
	return &scannedLinesQueue{
		q: queue.NewListQueue(),
	}
}

func (q *scannedLinesQueue) push(v scannedLine) {
	q.q.Push(v)
}

func (q *scannedLinesQueue) pop() (scannedLine, bool) {
	v := q.q.Pop()
	if v == nil {
		return scannedLine{}, false
	}

	return v.(scannedLine), true
}
