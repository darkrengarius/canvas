package canvas

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/darkrengarius/canvas/pkg/canvas/linescanfloodfiller"
	"gitlab.com/darkrengarius/canvas/pkg/canvas/simplefloodfiller"
	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

func TestNew(t *testing.T) {
	cfg := Config{
		W:  32,
		H:  12,
		FF: simplefloodfiller.New(),
	}

	wantM := make([][]byte, 0, cfg.H)
	for i := 0; i < cfg.H; i++ {
		wantRow := make([]byte, 0, cfg.W)
		for j := 0; j < cfg.W; j++ {
			wantRow = append(wantRow, DefaultBackgroundFill)
		}

		wantM = append(wantM, wantRow)
	}

	c := New(cfg)

	require.NotNil(t, c)
	require.Equal(t, cfg.W, c.w)
	require.Equal(t, cfg.H, c.h)
	require.Equal(t, wantM, c.data)
	require.Equal(t, cfg.FF, c.floodFiller)
}

func TestCanvas_Render(t *testing.T) {
	cfg := Config{
		W: 20,
		H: 20,
	}

	wantM := make([][]byte, 0, cfg.H)
	for i := 0; i < cfg.H; i++ {
		wantRow := make([]byte, 0, cfg.W)
		for j := 0; j < cfg.W; j++ {
			wantRow = append(wantRow, byte(i))
		}

		wantM = append(wantM, wantRow)
	}

	c := New(cfg)
	c.data = wantM

	gotM := c.Render()
	require.Equal(t, wantM, gotM)
}

func TestCanvas_PutRectangle(t *testing.T) {
	cfg := Config{
		W: 20,
		H: 20,
	}

	fill := byte('X')
	outline := byte('0')

	t.Run("rectangle starts out of canvas borders", func(t *testing.T) {
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: -1,
				Y: 2,
			},
			Width:   5,
			Height:  5,
			Fill:    &fill,
			Outline: &outline,
		}

		wantM := make([][]byte, 0, cfg.H)
		for i := 0; i < cfg.H; i++ {
			wantRow := make([]byte, 0, cfg.W)
			for j := 0; j < cfg.W; j++ {
				wantRow = append(wantRow, DefaultBackgroundFill)
			}

			wantM = append(wantM, wantRow)
		}

		c := New(cfg)
		c.PutRectangle(r)

		require.Equal(t, wantM, c.data)
	})

	t.Run("rectangle is completely within canvas", func(t *testing.T) {
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 2,
				Y: 2,
			},
			Width:  5,
			Height: 5,
		}

		t.Run("nil fill", func(t *testing.T) {
			r.Fill = nil
			r.Outline = &outline

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.X; i < (r.Start.X + r.Width); i++ {
				wantM[r.Start.Y][i] = outline
			}
			for i := r.Start.Y + 1; i < (r.Start.Y + r.Width - 1); i++ {
				wantM[i][r.Start.X] = outline
				wantM[i][r.Start.X+r.Width-1] = outline
			}
			for i := r.Start.X; i < (r.Start.X + r.Width); i++ {
				wantM[r.Start.Y+r.Height-1][i] = outline
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})

		t.Run("nil outline", func(t *testing.T) {
			r.Fill = &fill
			r.Outline = nil

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.Y; i < (r.Start.Y + r.Height); i++ {
				for j := r.Start.X; j < (r.Start.X + r.Width); j++ {
					wantM[i][j] = fill
				}
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})

		t.Run("both outline and fill specified", func(t *testing.T) {
			r.Fill = &fill
			r.Outline = &outline

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.X; i < (r.Start.X + r.Width); i++ {
				wantM[r.Start.Y][i] = outline
			}
			for i := r.Start.Y + 1; i < (r.Start.Y + r.Width - 1); i++ {
				wantM[i][r.Start.X] = outline
				wantM[i][r.Start.X+r.Width-1] = outline

				for j := r.Start.X + 1; j < (r.Start.X + r.Width - 1); j++ {
					wantM[i][j] = fill
				}
			}
			for i := r.Start.X; i < (r.Start.X + r.Width); i++ {
				wantM[r.Start.Y+r.Height-1][i] = outline
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})
	})

	t.Run("rectangle is partially out of canvas borders", func(t *testing.T) {
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 17,
				Y: 17,
			},
			Width:  5,
			Height: 5,
		}

		t.Run("nil fill", func(t *testing.T) {
			r.Fill = nil
			r.Outline = &outline

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.X; i < cfg.W; i++ {
				wantM[r.Start.Y][i] = outline
			}
			for i := r.Start.Y + 1; i < cfg.H; i++ {
				wantM[i][r.Start.X] = outline
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})

		t.Run("nil outline", func(t *testing.T) {
			r.Fill = &fill
			r.Outline = nil

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.Y; i < cfg.H; i++ {
				for j := r.Start.X; j < cfg.W; j++ {
					wantM[i][j] = fill
				}
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})

		t.Run("both outline and fill specified", func(t *testing.T) {
			r.Fill = &fill
			r.Outline = &outline

			wantM := make([][]byte, 0, cfg.H)
			for i := 0; i < cfg.H; i++ {
				wantRow := make([]byte, 0, cfg.W)
				for j := 0; j < cfg.W; j++ {
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				wantM = append(wantM, wantRow)
			}

			for i := r.Start.X; i < cfg.W; i++ {
				wantM[r.Start.Y][i] = outline
			}
			for i := r.Start.Y + 1; i < cfg.H; i++ {
				wantM[i][r.Start.X] = outline

				for j := r.Start.X + 1; j < cfg.W; j++ {
					wantM[i][j] = fill
				}
			}

			c := New(cfg)
			c.PutRectangle(r)

			require.Equal(t, wantM, c.data)
		})
	})
}

func TestCanvas_FloodFill(t *testing.T) {
	t.Run("line scan", func(t *testing.T) {
		testFloodFill(t, linescanfloodfiller.New())
	})

	t.Run("simple flood fill", func(t *testing.T) {
		testFloodFill(t, simplefloodfiller.New())
	})
}

func testFloodFill(t *testing.T, ff FloodFiller) {
	const (
		mSize    = 20
		fillChar = '-'
	)
	c := &Canvas{
		floodFiller: ff,
		w:           mSize,
		h:           mSize,
	}

	t.Run("fill empty canvas starting from left upper corner", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: 0,
				Y: 0,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("fill empty matrix starting from right lower corner", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: mSize - 1,
				Y: mSize - 1,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("fill empty matrix starting from around the center", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: mSize / 2,
				Y: mSize / 2,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("fill matrix with drawn rectangle starting from left upper corner", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		rOutline := byte('X')
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Width:  5,
			Height: 5,
		}

		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y][col] = rOutline
			wantM[r.Start.Y][col] = rOutline
		}
		for row := r.Start.Y + 1; row < (r.Start.Y + r.Height - 1); row++ {
			m[row][r.Start.X] = rOutline
			m[row][r.Start.X+r.Width-1] = rOutline

			wantM[row][r.Start.X] = rOutline
			wantM[row][r.Start.X+r.Width-1] = rOutline
			for col := r.Start.X + 1; col < (r.Start.X + r.Width - 1); col++ {
				wantM[row][col] = DefaultBackgroundFill
			}
		}
		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y+r.Height-1][col] = rOutline
			wantM[r.Start.Y+r.Height-1][col] = rOutline
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: 0,
				Y: 0,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("fill matrix with drawn rectangle starting from right lower corner", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		rOutline := byte('X')
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Width:  5,
			Height: 5,
		}

		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y][col] = rOutline
			wantM[r.Start.Y][col] = rOutline
		}
		for row := r.Start.Y + 1; row < (r.Start.Y + r.Height - 1); row++ {
			m[row][r.Start.X] = rOutline
			m[row][r.Start.X+r.Width-1] = rOutline

			wantM[row][r.Start.X] = rOutline
			wantM[row][r.Start.X+r.Width-1] = rOutline
			for col := r.Start.X + 1; col < (r.Start.X + r.Width - 1); col++ {
				wantM[row][col] = DefaultBackgroundFill
			}
		}
		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y+r.Height-1][col] = rOutline
			wantM[r.Start.Y+r.Height-1][col] = rOutline
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: mSize - 1,
				Y: mSize - 1,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("fill matrix with drawn rectangle starting from around the center", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, fillChar)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		rOutline := byte('X')
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Width:  5,
			Height: 5,
		}

		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y][col] = rOutline
			wantM[r.Start.Y][col] = rOutline
		}
		for row := r.Start.Y + 1; row < (r.Start.Y + r.Height - 1); row++ {
			m[row][r.Start.X] = rOutline
			m[row][r.Start.X+r.Width-1] = rOutline

			wantM[row][r.Start.X] = rOutline
			wantM[row][r.Start.X+r.Width-1] = rOutline
			for col := r.Start.X + 1; col < (r.Start.X + r.Width - 1); col++ {
				wantM[row][col] = DefaultBackgroundFill
			}
		}
		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y+r.Height-1][col] = rOutline
			wantM[r.Start.Y+r.Height-1][col] = rOutline
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: 12,
				Y: 12,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("having drawn rectangle with empty body flood-fill its outline", func(t *testing.T) {
		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, DefaultBackgroundFill)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		rOutline := byte('X')
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Width:  5,
			Height: 5,
		}

		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y][col] = rOutline
			wantM[r.Start.Y][col] = fillChar
		}
		for row := r.Start.Y + 1; row < (r.Start.Y + r.Height - 1); row++ {
			m[row][r.Start.X] = rOutline
			m[row][r.Start.X+r.Width-1] = rOutline

			wantM[row][r.Start.X] = fillChar
			wantM[row][r.Start.X+r.Width-1] = fillChar
		}
		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y+r.Height-1][col] = rOutline
			wantM[r.Start.Y+r.Height-1][col] = fillChar
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})

	t.Run("having drawn rectangle with empty body flood-fill its body", func(t *testing.T) {
		t.Run("fill canvas from the point out of canvas borders", func(t *testing.T) {
			m := make([][]byte, 0, mSize)
			wantM := make([][]byte, 0, mSize)
			for i := 0; i < mSize; i++ {
				row := make([]byte, 0, mSize)
				wantRow := make([]byte, 0, mSize)
				for j := 0; j < mSize; j++ {
					row = append(row, DefaultBackgroundFill)
					wantRow = append(wantRow, DefaultBackgroundFill)
				}

				m = append(m, row)
				wantM = append(wantM, wantRow)
			}

			c.data = m
			c.FloodFill(primitive.FloodFill{
				Start: primitive.Coords{
					X: -1,
					Y: -1,
				},
				Fill: 0,
			})

			require.Equal(t, wantM, c.data)
		})

		m := make([][]byte, 0, mSize)
		wantM := make([][]byte, 0, mSize)
		for i := 0; i < mSize; i++ {
			row := make([]byte, 0, mSize)
			wantRow := make([]byte, 0, mSize)
			for j := 0; j < mSize; j++ {
				row = append(row, DefaultBackgroundFill)
				wantRow = append(wantRow, DefaultBackgroundFill)
			}

			m = append(m, row)
			wantM = append(wantM, wantRow)
		}

		rOutline := byte('X')
		r := primitive.Rectangle{
			Start: primitive.Coords{
				X: 1,
				Y: 1,
			},
			Width:  5,
			Height: 5,
		}

		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y][col] = rOutline
			wantM[r.Start.Y][col] = rOutline
		}
		for row := r.Start.Y + 1; row < (r.Start.Y + r.Height - 1); row++ {
			m[row][r.Start.X] = rOutline
			m[row][r.Start.X+r.Width-1] = rOutline

			wantM[row][r.Start.X] = rOutline
			wantM[row][r.Start.X+r.Width-1] = rOutline
			for col := r.Start.X + 1; col < (r.Start.X + r.Width - 1); col++ {
				wantM[row][col] = fillChar
			}
		}
		for col := r.Start.X; col < (r.Start.X + r.Width); col++ {
			m[r.Start.Y+r.Height-1][col] = rOutline
			wantM[r.Start.Y+r.Height-1][col] = rOutline
		}

		c.data = m
		c.FloodFill(primitive.FloodFill{
			Start: primitive.Coords{
				X: 2,
				Y: 2,
			},
			Fill: fillChar,
		})

		require.Equal(t, wantM, c.data)
	})
}
