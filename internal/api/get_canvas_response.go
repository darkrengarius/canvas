package api

// GetCanvasResponse is a response for canvas fetching request.
type GetCanvasResponse struct {
	Rendered []string `json:"rendered"`
}
