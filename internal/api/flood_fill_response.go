package api

// FloodFillResponse is a response for `FloodFill`
// drawing operation.
type FloodFillResponse struct {
	CanvasID string `json:"canvas_id"`
}
