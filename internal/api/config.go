package api

// Config is a config for API.
type Config struct {
	Host string
	Port int
}
