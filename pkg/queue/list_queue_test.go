package queue

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNewListQueue(t *testing.T) {
	q := NewListQueue()
	require.NotNil(t, q)
	require.NotNil(t, q.data)
}

func TestListQueue_Push(t *testing.T) {
	q := NewListQueue()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		q.Push(v)
	}

	for _, v := range data {
		require.Equal(t, v, q.data.Head())
		q.data.RemoveFirst()
	}

	require.Nil(t, q.data.Head())
}

func TestListQueue_Peek(t *testing.T) {
	q := NewListQueue()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		q.Push(v)
	}

	for _, v := range data {
		require.Equal(t, v, q.Peek())
		// check that `Peek` call didn't modify the queue
		require.Equal(t, v, q.data.Head())

		q.data.RemoveFirst()
	}

	require.Nil(t, q.Peek())
}

func TestListQueue_Pop(t *testing.T) {
	q := NewListQueue()

	data := []interface{}{
		1,
		"data",
		'2',
	}

	for _, v := range data {
		q.Push(v)
	}

	for _, v := range data {
		require.Equal(t, v, q.Pop())
	}

	require.Nil(t, q.Peek())
}
