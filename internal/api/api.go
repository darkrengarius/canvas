package api

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	jsoniter "github.com/json-iterator/go"
	"github.com/sirupsen/logrus"

	"gitlab.com/darkrengarius/canvas/pkg/canvas"
	"gitlab.com/darkrengarius/canvas/pkg/db"
	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

var (
	// ErrInvalidHost is returned if specified host is invalid.
	ErrInvalidHost = errors.New("host is invalid")
	// ErrInvalidPort is returned if specified port is invalid.
	ErrInvalidPort = errors.New("port is invalid")
)

// API is a canvases manipulating HTTP API.
type API struct {
	cfg           Config
	log           *logrus.Logger
	canvasService *canvas.Service
	srv           *http.Server
	isServingMu   sync.Mutex
	isServing     bool

	canvasUpdatesMu sync.RWMutex
	canvasUpdates   map[string]map[string]chan []string
}

// New constructs new API instance.
func New(cfg Config, canvasService *canvas.Service) (*API, error) {
	if cfg.Host == "" {
		return nil, ErrInvalidHost
	}

	if cfg.Port <= 0 {
		return nil, ErrInvalidPort
	}

	log := logrus.New()
	log.SetLevel(logrus.DebugLevel)

	return &API{
		cfg:           cfg,
		log:           log,
		canvasService: canvasService,
		canvasUpdates: make(map[string]map[string]chan []string),
	}, nil
}

// ListenAndServe binds to the address and starts serving requests.
func (a *API) ListenAndServe() error {
	a.isServingMu.Lock()
	r := a.prepMux()
	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", a.cfg.Host, a.cfg.Port),
		Handler: r,
	}
	a.srv = srv
	a.isServing = true
	a.isServingMu.Unlock()

	a.log.Infof("Serving %s", srv.Addr)

	// http.ErrServerClosed might arise if the server performed graceful shutdown,
	// so we need to filter it out
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return err
	}

	return nil
}

// Shutdown performs graceful shutdown of API.
func (a *API) Shutdown(ctx context.Context) error {
	a.isServingMu.Lock()
	if !a.isServing {
		a.isServingMu.Unlock()
		return nil
	}
	srv := a.srv
	a.isServing = false
	a.isServingMu.Unlock()

	return srv.Shutdown(ctx)
}

func (a *API) prepMux() *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Handle("/ws", a.handleWS())

	r.Route("/api", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(a.canvasCtxMiddleware)

			r.Post("/rectangles", a.handlePutRectangle())
			r.Post("/flood-fills", a.handleFloodFill())
		})

		r.Get("/canvases/{id}", a.handleGetCanvas())
	})

	return r
}

func (a *API) handleGetCanvas() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")

		c, err := a.canvasService.Get(id)
		if err != nil {
			statusCode := http.StatusNotFound
			if !errors.Is(err, db.ErrNil) {
				statusCode = http.StatusInternalServerError
				a.log.WithError(err).Errorf("Failed to get canvas with ID %s", id)
			}
			http.Error(w, http.StatusText(statusCode), statusCode)
			return
		}

		resp := GetCanvasResponse{
			Rendered: c.RenderStrings(),
		}

		a.writeJSON(w, http.StatusOK, resp)
	}
}

func (a *API) handleFloodFill() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if contentType := r.Header.Get("Content-Type"); contentType != "application/json" {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		var f primitive.FloodFill
		if err := jsoniter.NewDecoder(r.Body).Decode(&f); err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		ctx := r.Context()
		c, ok := ctx.Value(contextKeyCanvas).(*canvas.Canvas)
		if !ok {
			// this totally shouldn't happen, but we log it just in case
			a.log.Errorln("Got wrong type of entity from request context")
			http.Error(w, http.StatusText(http.StatusUnprocessableEntity), http.StatusUnprocessableEntity)
			return
		}

		c.FloodFill(f)

		var err error
		canvasID := r.URL.Query().Get("canvas_id")
		canvasID, err = a.canvasService.AddOrUpdate(canvasID, c)
		if err != nil {
			a.log.WithError(err).Errorf("Failed to add-or-update canvas with ID %s", canvasID)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		// notify WS clients in goroutine, not to delay the response
		go a.pushCanvasUpdate(canvasID, c)

		resp := FloodFillResponse{
			CanvasID: canvasID,
		}

		a.writeJSON(w, http.StatusOK, resp)
	}
}

func (a *API) handlePutRectangle() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if contentType := r.Header.Get("Content-Type"); contentType != "application/json" {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		var rec primitive.Rectangle
		if err := jsoniter.NewDecoder(r.Body).Decode(&rec); err != nil {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		if !rec.IsValid() {
			http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
			return
		}

		ctx := r.Context()
		c, ok := ctx.Value(contextKeyCanvas).(*canvas.Canvas)
		if !ok {
			// this totally shouldn't happen, but we log it just in case
			a.log.Errorln("Got wrong type of entity from request context")
			http.Error(w, http.StatusText(http.StatusUnprocessableEntity), http.StatusUnprocessableEntity)
			return
		}

		c.PutRectangle(rec)

		var err error
		canvasID := r.URL.Query().Get("canvas_id")
		canvasID, err = a.canvasService.AddOrUpdate(canvasID, c)
		if err != nil {
			a.log.WithError(err).Errorf("Failed to add-or-update canvas with ID %s", canvasID)
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		// notify WS clients in goroutine, not to delay the response
		go a.pushCanvasUpdate(canvasID, c)

		resp := PutRectangleResponse{
			CanvasID: canvasID,
		}

		a.writeJSON(w, http.StatusOK, resp)
	}
}

func (a *API) handleWS() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		u := websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				// for the sake of simplicity, allow any origin
				return true
			},
		}
		conn, err := u.Upgrade(w, r, nil)
		if err != nil {
			a.log.WithError(err).Errorln("WS: Failed to upgrade connection")
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		defer func() {
			if err := conn.Close(); err != nil {
				a.log.WithError(err).Errorln("WS: Failed to close connection")
			}
		}()

		var req WSConnectionRequest
		if err := conn.ReadJSON(&req); err != nil {
			a.log.WithError(err).Errorln("WS: Failed to read/unmarshal connection request")
			return
		}

		c, err := a.canvasService.Get(req.CanvasID)
		if err != nil {
			a.log.WithError(err).Errorf("WS: Failed to get canvas with ID %s", req.CanvasID)
			return
		}

		clientID := uuid.New().String()

		updatesC := a.subscribeForCanvasUpdates(clientID, req.CanvasID)
		conn.SetCloseHandler(func(code int, text string) error {
			a.unsubscribeFromCanvasUpdates(clientID, req.CanvasID)
			return nil
		})

		updateMsg := GetCanvasResponse{
			Rendered: c.RenderStrings(),
		}
		if err := conn.WriteJSON(updateMsg); err != nil {
			a.log.WithError(err).Errorln("WS: Failed to marshal/write canvas update to connection")
			return
		}

		var closeOnce sync.Once
		closeC := make(chan struct{})
		defer closeOnce.Do(func() {
			// we close this with `sync.Once` cause it may be initiated
			// from within various parts of code but should be done once
			close(closeC)
		})
		go func() {
			const clientTimeout = 1 * time.Second

			for {
				select {
				case <-closeC:
					return
				default:
				}

				if err := conn.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
					a.log.WithError(err).Errorln("WS: Failed to ping client")
					closeOnce.Do(func() {
						close(closeC)
					})
					return
				}

				time.Sleep(clientTimeout)
			}
		}()

		for {
			select {
			case <-closeC:
				return
			case update := <-updatesC:
				updateMsg := GetCanvasResponse{
					Rendered: update,
				}

				if err := conn.WriteJSON(updateMsg); err != nil {
					a.log.WithError(err).Errorln("WS: Failed to marshal/write canvas update to connection")
					return
				}
			}
		}
	}
}

func (a *API) canvasCtxMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()

		canvasID := query.Get("canvas_id")
		c, err := a.canvasService.GetOrCreate(canvasID)
		if err != nil {
			statusCode := http.StatusNotFound
			if !errors.Is(err, db.ErrNil) {
				statusCode = http.StatusInternalServerError
				a.log.WithError(err).Errorf("Failed to get canvas with ID %s", canvasID)
			}
			http.Error(w, http.StatusText(statusCode), statusCode)
			return
		}

		ctx := context.WithValue(r.Context(), contextKeyCanvas, c)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func (a *API) writeJSON(w http.ResponseWriter, statusCode int, resp interface{}) {
	respBytes, err := jsoniter.ConfigFastest.Marshal(resp)
	if err != nil {
		a.log.WithError(err).Errorf("Failed to marshal response %v", resp)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	if _, err := w.Write(respBytes); err != nil {
		a.log.WithError(err).Errorln("Failed to write response to client")
	}
}

func (a *API) pushCanvasUpdate(canvasID string, c *canvas.Canvas) {
	// we keep it locked during all the notification process,
	// because reading side of channel is supposed to close it.
	// It's totally unsafe, so channel gets closed having mutex
	// acquired, this way we're good to go
	a.canvasUpdatesMu.RLock()
	if updatesCs, ok := a.canvasUpdates[canvasID]; ok {
		// since we're holding lock throughout all the process,
		// it's a good idea to check if we even need to render
		// the canvas (some clients are waiting for it), cause
		// rendering might be a heavy operation for big canvases
		if len(updatesCs) > 0 {
			rendered := c.RenderStrings()
			for _, updatesC := range updatesCs {
				updatesC <- rendered
			}
		}
	}
	a.canvasUpdatesMu.RUnlock()
}

func (a *API) subscribeForCanvasUpdates(clientID, canvasID string) chan []string {
	updatesC := make(chan []string)

	a.canvasUpdatesMu.Lock()
	if _, ok := a.canvasUpdates[canvasID]; !ok {
		a.canvasUpdates[canvasID] = make(map[string]chan []string)
	}
	a.canvasUpdates[canvasID][clientID] = updatesC
	a.canvasUpdatesMu.Unlock()

	return updatesC
}

func (a *API) unsubscribeFromCanvasUpdates(clientID, canvasID string) {
	a.canvasUpdatesMu.Lock()
	defer a.canvasUpdatesMu.Unlock()

	if _, ok := a.canvasUpdates[canvasID]; !ok {
		return
	}

	updatesC, ok := a.canvasUpdates[canvasID][clientID]
	if !ok {
		return
	}

	close(updatesC)
	delete(a.canvasUpdates[canvasID], clientID)
}
