module gitlab.com/darkrengarius/canvas

go 1.14

require (
	github.com/go-chi/chi v1.5.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/json-iterator/go v1.1.10
	github.com/onsi/ginkgo v1.14.2 // indirect
	github.com/onsi/gomega v1.10.3 // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1
)
