// Package simplefloodfiller provides implementation of `canvas.FloodFiller`
// which utilizes basic flood-filling algorithm.
package simplefloodfiller

import (
	"sync"

	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

var stackPool = sync.Pool{
	New: func() interface{} { return newCoordsStack() },
}

// SimpleFloodFiller implements `canvas.FloodFiller`
// utilizing basic flood-filling algorithm.
type SimpleFloodFiller struct {
}

// New constructs new `SimpleFloodFiller`.
func New() *SimpleFloodFiller {
	return &SimpleFloodFiller{}
}

// FloodFill implements `canvas.FloodFiller`. Uses basic flood-filling algorithm. The only difference is
// that it's based on the software stack instead of a hardware one to avoid stack overflows.
func (f *SimpleFloodFiller) FloodFill(m [][]byte, start primitive.Coords, replaceChar, fillChar byte) {
	if start.Y < 0 || start.Y >= len(m) ||
		start.X < 0 || start.X >= len(m[start.Y]) ||
		m[start.Y][start.X] != replaceChar {
		return
	}

	s := stackPool.Get().(*coordsStack)
	defer func() {
		s.clear()
		stackPool.Put(s)
	}()

	s.push(primitive.Coords{
		X: start.X,
		Y: start.Y,
	})

	for coords, ok := s.pop(); ok; coords, ok = s.pop() {
		m[coords.Y][coords.X] = fillChar

		// we preemptively check if next element must be replaced to avoid unnecessary
		// stack functions calls
		if (coords.X+1) >= 0 && (coords.X+1) < len(m[coords.Y]) && m[coords.Y][coords.X+1] == replaceChar {
			s.push(primitive.Coords{
				X: coords.X + 1,
				Y: coords.Y,
			})
		}

		if (coords.X-1) >= 0 && (coords.X-1) < len(m[coords.Y]) && m[coords.Y][coords.X-1] == replaceChar {
			s.push(primitive.Coords{
				X: coords.X - 1,
				Y: coords.Y,
			})
		}

		if (coords.Y+1) >= 0 && (coords.Y+1) < len(m) && m[coords.Y+1][coords.X] == replaceChar {
			s.push(primitive.Coords{
				X: coords.X,
				Y: coords.Y + 1,
			})
		}

		if (coords.Y-1) >= 0 && (coords.Y-1) < len(m) && m[coords.Y-1][coords.X] == replaceChar {
			s.push(primitive.Coords{
				X: coords.X,
				Y: coords.Y - 1,
			})
		}
	}
}
