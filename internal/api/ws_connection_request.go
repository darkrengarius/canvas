package api

// WSConnectionRequest is sent by WS client to subscribe for updates
// of the canvas.
type WSConnectionRequest struct {
	CanvasID string `json:"canvas_id"`
}
