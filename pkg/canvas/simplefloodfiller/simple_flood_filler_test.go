package simplefloodfiller

import (
	"testing"

	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

func BenchmarkSimpleFloodFiller_FloodFill(b *testing.B) {
	const size = 20

	m := make([][]byte, 0, size)
	for i := 0; i < size; i++ {
		row := make([]byte, 0, size)
		for j := 0; j < size; j++ {
			row = append(row, ' ')
		}

		m = append(m, row)
	}

	ff := New()

	const (
		replaceChar = ' '
		fillChar    = '-'
	)
	coords := primitive.Coords{
		X: 0,
		Y: 0,
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		ff.FloodFill(m, coords, replaceChar, fillChar)
	}
}
