package db

// RedisConfiguration is a configuration for Redis.
type RedisConfiguration struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Password string `json:"password"`
}
