package queue

import "gitlab.com/darkrengarius/canvas/pkg/list"

// ListQueue is a queue utilizing linked list as a
// base data storage.
type ListQueue struct {
	data *list.SinglyLinked
}

// NewListQueue constructs new `ListQueue`.
func NewListQueue() *ListQueue {
	return &ListQueue{
		data: list.NewSinglyLinked(),
	}
}

// Push adds item `v` to the back of the queue.
func (q *ListQueue) Push(v interface{}) {
	q.data.AddLast(v)
}

// Peek gets item from the head of the queue.
func (q *ListQueue) Peek() interface{} {
	return q.data.Head()
}

// Pop gets and removes item from the head of the queue.
func (q *ListQueue) Pop() interface{} {
	v := q.Peek()

	q.data.RemoveFirst()

	return v
}
