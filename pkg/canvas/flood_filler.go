package canvas

import "gitlab.com/darkrengarius/canvas/pkg/primitive"

// FloodFiller is a generic definition of an algorithm implementing flood fill operation.
type FloodFiller interface {
	// FloodFill flood-fills matrix `m` replacing all encountered `replaceChar` elements
	// with `fillChar`. Search goes on from `start` in 4 directions: up, left, down, right.
	// If search encounters element not equal to `replaceChar` it stops moving in that particular
	// direction.
	FloodFill(m [][]byte, start primitive.Coords, replaceChar, fillChar byte)
}
