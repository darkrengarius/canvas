package redisrepo

import (
	"fmt"

	"github.com/go-redis/redis"
	"github.com/google/uuid"

	"gitlab.com/darkrengarius/canvas/pkg/canvas"
	"gitlab.com/darkrengarius/canvas/pkg/db"
)

// CanvasRepository is a storage of canvases which utilizes
// Redis as a backend storage.
// Implements `canvas.Repository`.
type CanvasRepository struct {
	cl redis.UniversalClient
}

// NewCanvasRepository constructs new canvas repository.
func NewCanvasRepository(cl redis.UniversalClient) *CanvasRepository {
	return &CanvasRepository{
		cl: cl,
	}
}

// Canvas gets canvas by its `id`.
func (r *CanvasRepository) Canvas(id string) (*canvas.Canvas, error) {
	cJSON, err := r.cl.HGet("canvas", id).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, db.ErrNil
		}

		return nil, fmt.Errorf("failed to get data: %w", err)
	}

	var c canvas.Canvas

	if err := c.UnmarshalJSON([]byte(cJSON)); err != nil {
		return nil, fmt.Errorf("failed to unmarshal data: %w", err)
	}

	return &c, nil
}

// Add stores new canvas `c`.
func (r *CanvasRepository) Add(c *canvas.Canvas) (string, error) {
	id := uuid.New().String()

	if err := r.store(id, c); err != nil {
		return "", err
	}

	return id, nil
}

// Update updates existing canvas with id `id`.
func (r *CanvasRepository) Update(id string, c *canvas.Canvas) error {
	return r.store(id, c)
}

func (r *CanvasRepository) store(id string, c *canvas.Canvas) error {
	cJSON, err := c.MarshalJSON()
	if err != nil {
		return fmt.Errorf("failed to marshal data: %w", err)
	}

	if err := r.cl.HSet("canvas", id, string(cJSON)).Err(); err != nil {
		return fmt.Errorf("failed to store data: %w", err)
	}

	return nil
}
