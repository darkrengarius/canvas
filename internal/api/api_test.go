package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/darkrengarius/canvas/pkg/canvas"
	"gitlab.com/darkrengarius/canvas/pkg/canvas/simplefloodfiller"
	"gitlab.com/darkrengarius/canvas/pkg/db"
	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

func TestNew(t *testing.T) {
	type want struct {
		err error
	}

	tests := []struct {
		name string
		cfg  Config
		want want
	}{
		{
			name: "ok",
			cfg: Config{
				Host: "localhost",
				Port: 5000,
			},
			want: want{
				err: nil,
			},
		},
		{
			name: "invalid host",
			cfg: Config{
				Host: "",
				Port: 5000,
			},
			want: want{
				err: ErrInvalidHost,
			},
		},
		{
			name: "invalid port",
			cfg: Config{
				Host: "localhost",
				Port: 0,
			},
			want: want{
				err: ErrInvalidPort,
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			a, err := New(tc.cfg, nil)
			require.Equal(t, tc.want.err, err)
			if err != nil {
				return
			}

			require.NotNil(t, a)
			require.Equal(t, a.cfg, tc.cfg)
			require.NotNil(t, a.log)
		})
	}
}

func TestAPI_handleGetCanvas(t *testing.T) {
	type want struct {
		status int
		resp   GetCanvasResponse
	}

	apiCfg := Config{
		Host: "localhost",
		Port: 5000,
	}

	canvasCfg := canvas.Config{
		W:  20,
		H:  20,
		FF: simplefloodfiller.New(),
	}
	c := canvas.New(canvasCfg)

	tests := []struct {
		name               string
		canvasID           string
		repoReturnedCanvas *canvas.Canvas
		repoReturnedErr    error
		want               want
	}{
		{
			name:               "200",
			canvasID:           "1",
			repoReturnedCanvas: c,
			repoReturnedErr:    nil,
			want: want{
				status: http.StatusOK,
				resp: GetCanvasResponse{
					Rendered: c.RenderStrings(),
				},
			},
		},
		{
			name:               "404",
			canvasID:           "1",
			repoReturnedCanvas: nil,
			repoReturnedErr:    db.ErrNil,
			want: want{
				status: http.StatusNotFound,
				resp:   GetCanvasResponse{},
			},
		},
		{
			name:               "500",
			canvasID:           "1",
			repoReturnedCanvas: nil,
			repoReturnedErr:    assert.AnError,
			want: want{
				status: http.StatusInternalServerError,
				resp:   GetCanvasResponse{},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			req, err := http.NewRequest(http.MethodGet, "/api/canvases/"+tc.canvasID, nil)
			require.NoError(t, err)

			rr := httptest.NewRecorder()

			repo := &canvas.MockRepository{}
			repo.On("Canvas", tc.canvasID).Return(tc.repoReturnedCanvas, tc.repoReturnedErr)

			canvasService := canvas.NewService(canvasCfg, repo)

			a, err := New(apiCfg, canvasService)
			require.NoError(t, err)

			router := a.prepMux()

			router.ServeHTTP(rr, req)

			status := rr.Code
			require.Equal(t, tc.want.status, status, "got %v, want %v", status, tc.want.status)
			if status != http.StatusOK {
				return
			}

			var resp GetCanvasResponse
			err = json.NewDecoder(rr.Body).Decode(&resp)
			require.NoError(t, err)
			require.Equal(t, tc.want.resp, resp)
		})
	}
}

func TestAPI_handleFloodFill(t *testing.T) {
	type want struct {
		status int
		resp   FloodFillResponse
	}

	apiCfg := Config{
		Host: "localhost",
		Port: 5000,
	}

	canvasCfg := canvas.Config{
		W:  20,
		H:  20,
		FF: simplefloodfiller.New(),
	}

	validFloodFill := primitive.FloodFill{
		Start: primitive.Coords{
			X: 0,
			Y: 0,
		},
		Fill: 'X',
	}

	const applicationJSONContentType = "application/json"

	tests := []struct {
		name                 string
		contentType          string
		body                 []byte
		floodFill            primitive.FloodFill
		canvasID             string
		repoReturnedCanvasID string
		repoReturnedErr      error
		want                 want
	}{
		{
			name:                 "200 (add new canvas)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validFloodFill),
			floodFill:            validFloodFill,
			canvasID:             "",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      nil,
			want: want{
				status: http.StatusOK,
				resp: FloodFillResponse{
					CanvasID: "1",
				},
			},
		},
		{
			name:                 "200 (update existing canvas)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validFloodFill),
			floodFill:            validFloodFill,
			canvasID:             "1",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      nil,
			want: want{
				status: http.StatusOK,
				resp: FloodFillResponse{
					CanvasID: "1",
				},
			},
		},
		{
			name:        "400 (missing/wrong content-type)",
			contentType: "invalid",
			body:        marshalJSON(t, validFloodFill),
			floodFill:   validFloodFill,
			canvasID:    "1",
			want: want{
				status: http.StatusBadRequest,
				resp:   FloodFillResponse{},
			},
		},
		{
			name:        "400 (invalid body)",
			contentType: applicationJSONContentType,
			body:        nil,
			canvasID:    "1",
			want: want{
				status: http.StatusBadRequest,
				resp:   FloodFillResponse{},
			},
		},
		{
			name:                 "500 (internal repo error)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validFloodFill),
			floodFill:            validFloodFill,
			canvasID:             "",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      assert.AnError,
			want: want{
				status: http.StatusInternalServerError,
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			url := "/api/flood-fills"
			if tc.canvasID != "" {
				url += "?canvas_id=" + tc.canvasID
			}

			req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(tc.body))
			require.NoError(t, err)

			if tc.contentType != "" {
				req.Header.Set("Content-Type", tc.contentType)
			}

			rr := httptest.NewRecorder()

			c := canvas.New(canvasCfg)

			floodFilledC := canvas.New(canvasCfg)
			floodFilledC.FloodFill(tc.floodFill)

			repo := &canvas.MockRepository{}
			if tc.canvasID == "" {
				repo.On("Add", floodFilledC).Return(tc.repoReturnedCanvasID, tc.repoReturnedErr)
			} else {
				repo.On("Canvas", tc.canvasID).Return(c, tc.repoReturnedErr)
				repo.On("Update", tc.canvasID, floodFilledC).Return(tc.repoReturnedErr)
			}

			canvasService := canvas.NewService(canvasCfg, repo)

			a, err := New(apiCfg, canvasService)
			require.NoError(t, err)

			router := a.prepMux()

			router.ServeHTTP(rr, req)

			status := rr.Code
			require.Equal(t, tc.want.status, status, "got %v, want %v", status, tc.want.status)
			if status != http.StatusOK {
				return
			}

			var resp FloodFillResponse
			err = json.NewDecoder(rr.Body).Decode(&resp)
			require.NoError(t, err)
			require.Equal(t, tc.want.resp, resp)
		})
	}
}

func TestAPI_handlePutRectangle(t *testing.T) {
	type want struct {
		status int
		resp   PutRectangleResponse
	}

	apiCfg := Config{
		Host: "localhost",
		Port: 5000,
	}

	canvasCfg := canvas.Config{
		W:  20,
		H:  20,
		FF: simplefloodfiller.New(),
	}

	fill := byte('X')
	validRectangle := primitive.Rectangle{
		Start: primitive.Coords{
			X: 0,
			Y: 0,
		},
		Width:  5,
		Height: 5,
		Fill:   &fill,
	}
	invalidRectangle := primitive.Rectangle{
		Start:   primitive.Coords{},
		Width:   0,
		Height:  0,
		Fill:    nil,
		Outline: nil,
	}

	const applicationJSONContentType = "application/json"

	tests := []struct {
		name                 string
		contentType          string
		body                 []byte
		rectangle            primitive.Rectangle
		canvasID             string
		repoReturnedCanvasID string
		repoReturnedErr      error
		want                 want
	}{
		{
			name:                 "200 (add new canvas)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validRectangle),
			rectangle:            validRectangle,
			canvasID:             "",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      nil,
			want: want{
				status: http.StatusOK,
				resp: PutRectangleResponse{
					CanvasID: "1",
				},
			},
		},
		{
			name:                 "200 (update existing canvas)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validRectangle),
			rectangle:            validRectangle,
			canvasID:             "1",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      nil,
			want: want{
				status: http.StatusOK,
				resp: PutRectangleResponse{
					CanvasID: "1",
				},
			},
		},
		{
			name:        "400 (missing/wrong content-type)",
			contentType: "invalid",
			body:        marshalJSON(t, validRectangle),
			rectangle:   validRectangle,
			canvasID:    "1",
			want: want{
				status: http.StatusBadRequest,
				resp:   PutRectangleResponse{},
			},
		},
		{
			name:        "400 (invalid body)",
			contentType: applicationJSONContentType,
			body:        nil,
			canvasID:    "1",
			want: want{
				status: http.StatusBadRequest,
				resp:   PutRectangleResponse{},
			},
		},
		{
			name:        "400 (invalid rectangle)",
			contentType: applicationJSONContentType,
			body:        marshalJSON(t, invalidRectangle),
			canvasID:    "1",
			want: want{
				status: http.StatusBadRequest,
				resp:   PutRectangleResponse{},
			},
		},
		{
			name:                 "500 (internal repo error)",
			contentType:          applicationJSONContentType,
			body:                 marshalJSON(t, validRectangle),
			rectangle:            validRectangle,
			canvasID:             "",
			repoReturnedCanvasID: "1",
			repoReturnedErr:      assert.AnError,
			want: want{
				status: http.StatusInternalServerError,
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			if tc.name == "400 (invalid body)" {
				fmt.Println()
			}
			url := "/api/rectangles"
			if tc.canvasID != "" {
				url += "?canvas_id=" + tc.canvasID
			}

			req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(tc.body))
			require.NoError(t, err)

			if tc.contentType != "" {
				req.Header.Set("Content-Type", tc.contentType)
			}

			rr := httptest.NewRecorder()

			c := canvas.New(canvasCfg)

			modifiedC := canvas.New(canvasCfg)
			if tc.rectangle.IsValid() {
				modifiedC.PutRectangle(tc.rectangle)
			}

			repo := &canvas.MockRepository{}
			if tc.canvasID == "" {
				repo.On("Add", modifiedC).Return(tc.repoReturnedCanvasID, tc.repoReturnedErr)
			} else {
				repo.On("Canvas", tc.canvasID).Return(c, tc.repoReturnedErr)
				repo.On("Update", tc.canvasID, modifiedC).Return(tc.repoReturnedErr)
			}

			canvasService := canvas.NewService(canvasCfg, repo)

			a, err := New(apiCfg, canvasService)
			require.NoError(t, err)

			router := a.prepMux()

			router.ServeHTTP(rr, req)

			status := rr.Code
			require.Equal(t, tc.want.status, status, "got %v, want %v", status, tc.want.status)
			if status != http.StatusOK {
				return
			}

			var resp PutRectangleResponse
			err = json.NewDecoder(rr.Body).Decode(&resp)
			require.NoError(t, err)
			require.Equal(t, tc.want.resp, resp)
		})
	}
}

func marshalJSON(t *testing.T, v interface{}) []byte {
	data, err := json.Marshal(v)
	require.NoError(t, err)

	return data
}
