package canvas

import "gitlab.com/darkrengarius/canvas/pkg/canvas/simplefloodfiller"

// Config is a configuration for canvas.
type Config struct {
	W  int
	H  int
	FF FloodFiller
}

// DefaultConfig gets default canvas config.
func DefaultConfig() Config {
	return Config{
		W:  50,
		H:  50,
		FF: simplefloodfiller.New(),
	}
}
