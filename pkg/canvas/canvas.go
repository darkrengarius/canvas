package canvas

import (
	jsoniter "github.com/json-iterator/go"

	"gitlab.com/darkrengarius/canvas/pkg/primitive"
)

// DefaultBackgroundFill is a character used to fill canvas background by default.
const DefaultBackgroundFill = byte(' ')

// Canvas is an canvas for drawing with ASCII characters.
type Canvas struct {
	w           int
	h           int
	floodFiller FloodFiller
	data        [][]byte
}

// New constructs new canvas of width `w` and height `h`.
func New(cfg Config) *Canvas {
	data := make([][]byte, 0, cfg.H)
	for i := 0; i < cfg.H; i++ {
		row := make([]byte, 0, cfg.W)
		for j := 0; j < cfg.W; j++ {
			row = append(row, DefaultBackgroundFill)
		}

		data = append(data, row)
	}

	return &Canvas{
		w:           cfg.W,
		h:           cfg.H,
		floodFiller: cfg.FF,
		data:        data,
	}
}

// SetFloodFiller sets flood filler implementation to the canvas.
func (c *Canvas) SetFloodFiller(ff FloodFiller) {
	c.floodFiller = ff
}

// PutRectangle puts rectangle `r` to the canvas.
func (c *Canvas) PutRectangle(r primitive.Rectangle) {
	if r.Start.X < 0 || r.Start.Y < 0 ||
		r.Start.X >= c.w || r.Start.Y >= c.h {
		return
	}

	var outline byte
	if r.Outline != nil {
		outline = *r.Outline
	} else {
		outline = *r.Fill
	}

	if r.Start.Y < c.h {
		// fill upper border
		c.putLine(r.Start.Y, r.Start.X, r.Width, outline)
	}

	// fill body between upper and lower borders
	for row := r.Start.Y + 1; row < c.h && row < (r.Start.Y+r.Height-1); row++ {
		// left border element
		c.putLine(row, r.Start.X, 1, outline)

		if r.Fill != nil {
			// inner body elements
			c.putLine(row, r.Start.X+1, r.Width-2, *r.Fill)
		}

		// right border element
		c.putLine(row, r.Start.X+r.Width-1, 1, outline)
	}

	if (r.Start.Y + r.Height - 1) < c.h {
		// fill lower border
		c.putLine(r.Start.Y+r.Height-1, r.Start.X, r.Width, outline)
	}
}

// FloodFill flood-fills canvas based on parameters `f`.
func (c *Canvas) FloodFill(f primitive.FloodFill) {
	if f.Start.X < 0 || f.Start.X >= c.w ||
		f.Start.Y < 0 || f.Start.Y >= c.h ||
		c.data[f.Start.Y][f.Start.X] == f.Fill {
		return
	}

	replaceChar := c.data[f.Start.Y][f.Start.X]

	c.floodFiller.FloodFill(c.data, f.Start, replaceChar, f.Fill)
}

// Render renders canvas data into matrix of bytes.
func (c *Canvas) Render() [][]byte {
	return c.data
}

// RenderStrings renders canvas data into array of strings.
func (c *Canvas) RenderStrings() []string {
	rows := make([]string, 0, c.h)
	for row := range c.data {
		rows = append(rows, string(c.data[row]))
	}

	return rows
}

// MarshalJSON implements `json.Marshaler`.
func (c *Canvas) MarshalJSON() ([]byte, error) {
	return jsoniter.ConfigFastest.Marshal(c.toMarshalable())
}

// UnmarshalJSON implements `json.Unmarshaler`.
func (c *Canvas) UnmarshalJSON(data []byte) error {
	var unmarshaled marshalable
	if err := jsoniter.ConfigFastest.Unmarshal(data, &unmarshaled); err != nil {
		return err
	}

	*c = *(unmarshaled.toCanvas())

	return nil
}

func (c *Canvas) putLine(row, colStart, lineLen int, char byte) {
	for col := colStart; col < c.w && col < (colStart+lineLen); col++ {
		c.data[row][col] = char
	}
}

func (c *Canvas) toMarshalable() *marshalable {
	return &marshalable{
		W:    c.w,
		H:    c.h,
		Data: c.data,
	}
}
