var JSONRPCClient = (function () {
    function JSONRPCClient() {
    }
    JSONRPCClient.prototype.connect = function (url) {
        var _this = this;
        this.client = this;
        this.socket = new WebSocket(url);
        var client = this;
        this.socket.onmessage = function (event) {
            var data = JSON.parse(event.data);
            if (data.rendered) {
                renderCanvas(data.rendered);
            }
        };
        this.socket.onclose = function (event) {
            if (event.wasClean) {
                console.log('Clean connection close');
            }
            else {
                console.log('Dicsonnection');
            }
            console.log('Code: ' + event.code + ' cause: ' + event.reason);
        };
        return new Promise(function (resolve, reject) {
            _this.socket.onerror = function (error) { return reject(error); };
            _this.socket.onopen = function () {
                _this.socket.onerror = function (error) {
                    console.log("Error " + error);
                };
                resolve();
            };
        });
    };
    JSONRPCClient.prototype.sendCanvasId = function (canvasId) {
        this.socket.send(`{"canvas_id":"${canvasId}"}`);
    };
    return JSONRPCClient;
}());